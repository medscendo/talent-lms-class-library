﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentLmsClassLibrary;
using System.Linq;

namespace TalentLmsClassLibraryTest
{
    [TestClass]
    public class Tests
    {
        #region Courses
        Courses courses = new Courses("https://healthmonix.talentlms.com/api/v1", "YyDLu8gLfxpwFh4nAv7H4iyRxuuIhm" );

        [TestMethod]
        public void GetCoursesNoNulls()
        {
            var allCourses = new CoursesModel();
            allCourses = courses.GetCourses();
            var exists = allCourses.Courses.Count > 0 ? true : false;

            Assert.AreEqual(true, exists);
        }

        [TestMethod]
        public void GetCourse()
        {
            var course = new CourseModel();
            var courseId = 128;
            course = courses.GetCourse(courseId);

            Assert.AreEqual(course.Id, courseId);
        }

        [TestMethod]
        public void GoToCourse()
        {
            var userId = 9;
            var courseId = 128;
            var goto_url = courses.GoToCourse(userId, courseId);

            Assert.IsNotNull(goto_url.Goto_Url);
        }

        [TestMethod]
        public void AddUserToCourse()
        {
            var userRegistration = new UserRegistrationModel()
            {
                User_Id = 9,
                Course_Id = 127,
                Role = "learner"
            };
            //For whatever reason, this endpoint returns an array, though it's always one element,
            //as you can't add a user to multiple courses.
            var userId = 9;
            var courseId = 127;
            var response = courses.AddUserToCourse(userRegistration).UserRegistrations.FirstOrDefault();
            
            Assert.AreEqual(userId, response.User_Id);
            Assert.AreEqual(courseId, response.Course_Id);
        }

        [TestMethod]
        public void RemoveUserFromCourse()
        {
            var userId = 9;
            var courseId = 127;
            var response = courses.RemoveUserFromCourse(userId, courseId);

            Assert.AreEqual(userId, response.User_Id);
            Assert.AreEqual(courseId, response.Course_Id);
        }
        #endregion

        #region Users
        Users users = new Users("https://healthmonix.talentlms.com/api/v1", "YyDLu8gLfxpwFh4nAv7H4iyRxuuIhm");
        [TestMethod]
        public void GetUsersNoNulls()
        {
            var allUsers = new UsersModel();
            allUsers = users.GetUsers();
            var exists = allUsers.Users.Count > 0 ? true : false;

            Assert.AreEqual(true, exists);
        }

        [TestMethod]
        public void GetUserById()
        {
            var user = new UserModel();
            var userId = 9;
            user = users.GetUser(userId);

            Assert.AreEqual(user.Id, userId);
        }

        [TestMethod]
        public void GetUserByEmail()
        {
            var user = new UserModel();
            var email = "jhall@healthmonix.com";
            user = users.GetUser(email);

            Assert.AreEqual(user.Email, email);
        }

        [TestMethod]
        public void AddNewUser()
        {
            var newUser = new NewUserModel(){
                First_Name = "Johnny5",
                Last_Name = "Test5",
                Email = "jhall5@healthmonix.com",
                Login = "jtest5",
                Password = "jtest123",
                Branch_Id = 1,
                Group_Id = 2
            };
            var response = users.UserSignUp(newUser);
            var newUserId = response.Id;

            Assert.AreNotEqual(0, response.Id);
        }

        [TestMethod]
        public void EditUser()
        {
            var user = new EditUserModel()
            {
                User_Id = 15,
                First_Name = "Johnny6",
                Last_Name = "Test6",
                Email = "jhall6@healthmonix.com",
                Login = "jtest6",
                Password = "jtest123",
                Bio = "I Like cheese.",
                Timezone = "-5" //[-12, 12] range; -5 is EST
            };
            var response = users.EditUser(user);
            var userId = response.Id;

            Assert.AreEqual(userId, response.Id);
        }

        [TestMethod]
        public void DeleteUser()
        {
            var userId = 15;
            var deletedByUserId = 9;
            var response = users.DeleteUser(userId, deletedByUserId);
            var message = "Operation completed successfully";

            Assert.AreEqual(message, response.Message);
        }

        [TestMethod]
        public void LoginUser()
        {
            var login = "jhall";
            var password = "jhall!123";
            var logout_redirect = "";
            var response = users.UserLogin(login, password, logout_redirect);

            Assert.IsNotNull(response.Login_Key);
        }

        [TestMethod]
        public void LogoutUser()
        {
            var userId = 9;
            var next = "https://www.google.com/search?source=hp&ei=8SVFWsKYMcm0_AaokoOIDA&q=nintendo&oq=nintendo&gs_l=psy-ab.3..0i131k1l4j0l3j0i131k1j0l2.2534.3951.0.4154.9.4.0.4.4.0.98.298.4.4.0....0...1c.1.64.psy-ab..1.8.329.0...0.XsaAnkR5Brc";
            var response = users.UserLogout(userId, next);

            Assert.IsNotNull(response.Redirect_Url);
        }

        [TestMethod]
        public void SetUserStatus()
        {
            var userStatus = new UserStatusModel();
            var userId = 14;
            var status = "inactive";
            userStatus = users.SetUserStatus(userId, status);

            Assert.AreEqual(userStatus.User_Id, userId);
            Assert.AreEqual(userStatus.Status, status);
        }

        [TestMethod]
        public void ForgotUsername()
        {
            var userIdReturn = new ForgotUsernamePasswordModel();
            var userId = 9;
            var email = "jhall@healthmonix.com";
            var domain_Url = "www.ign.com";
            userIdReturn = users.ForgotUsername(email, domain_Url);
            Assert.AreEqual(userIdReturn.User_Id, userId);
        }

        [TestMethod]
        public void ForgotPassword()
        {
            var userIdReturn = new ForgotUsernamePasswordModel();
            var username = "jhall";
            var domain_Url = "www.google.com";
            var redirect_Url = "www.apple.com";
            userIdReturn = users.ForgotPassword(username, domain_Url, redirect_Url);
            Assert.AreNotEqual(0, userIdReturn.User_Id);
        }
        #endregion
    }
}
