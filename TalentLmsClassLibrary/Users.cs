﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net;

namespace TalentLmsClassLibrary
{
    public class Users
    {
        private string _authKey;
        private string _domain;

        public Users(string domain, string authKey)
        {
            _domain = domain;
            _authKey = authKey;
        }

        private IRestResponse AuthorizeGet(string endpoint)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var path = endpoint;
            var username = _authKey;
            var password = "";

            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));
            var client = new RestClient(path);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", $"Basic {encoded}");
            IRestResponse response = client.Execute(request);

            return response;
        }

        private string Base64Encode(string url)
        {
            var convertRedirect = System.Text.Encoding.UTF8.GetBytes(url);
            var encodedRedirect = System.Convert.ToBase64String(convertRedirect);
            return encodedRedirect;
        }

        public UsersModel GetUsers()
        {
            try
            {
                var path = $"{_domain}/users";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var users = new UsersModel();
                users.Users = JsonConvert.DeserializeObject<List<UserModel>>(result);
                return users;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UsersModel();
            return empty;
        }

        public UserModel GetUser(int id)
        {
            try
            {
                var path = $"{_domain}/users/id:{id}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var user = new UserModel();
                user = JsonConvert.DeserializeObject<UserModel>(result);
                JObject o = JObject.Parse(result);
                foreach (var item in o)
                {
                    if (item.Key == "courses")
                    {
                        var courses = item.Value.ToString();
                        user.CourseUsersModel.CourseUsers = JsonConvert.DeserializeObject<List<CourseUserModel>>(courses);
                    }
                    if (item.Key == "branches")
                    {
                        var branches = item.Value.ToString();
                        user.BranchesModel.Branches = JsonConvert.DeserializeObject<List<BranchModel>>(branches);
                    }
                    if (item.Key == "groups")
                    {
                        var groups = item.Value.ToString();
                        user.GroupsModel.Groups = JsonConvert.DeserializeObject<List<GroupModel>>(groups);
                    }
                    if (item.Key == "certifications")
                    {
                        var certifications = item.Value.ToString();
                        user.CertificationsModel.Certifications = JsonConvert.DeserializeObject<List<CertificationModel>>(certifications);
                    }
                    if (item.Key == "badges")
                    {
                        var badges = item.Value.ToString();
                        user.BadgesModel.Badges = JsonConvert.DeserializeObject<List<BadgeModel>>(badges);
                    }
                }
                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserModel();
            return empty;
        }

        public UserModel GetUser(string email)
        {
            try
            {
                var path = $"{_domain}/users/email:{email}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var user = new UserModel();
                user = JsonConvert.DeserializeObject<UserModel>(result);
                JObject o = JObject.Parse(result);
                foreach (var item in o)
                {
                    if (item.Key == "courses")
                    {
                        var courses = item.Value.ToString();
                        user.CourseUsersModel.CourseUsers = JsonConvert.DeserializeObject<List<CourseUserModel>>(courses);
                    }
                    if (item.Key == "branches")
                    {
                        var branches = item.Value.ToString();
                        user.BranchesModel.Branches = JsonConvert.DeserializeObject<List<BranchModel>>(branches);
                    }
                    if (item.Key == "groups")
                    {
                        var groups = item.Value.ToString();
                        user.GroupsModel.Groups = JsonConvert.DeserializeObject<List<GroupModel>>(groups);
                    }
                    if (item.Key == "certifications")
                    {
                        var certifications = item.Value.ToString();
                        user.CertificationsModel.Certifications = JsonConvert.DeserializeObject<List<CertificationModel>>(certifications);
                    }
                    if (item.Key == "badges")
                    {
                        var badges = item.Value.ToString();
                        user.BadgesModel.Badges = JsonConvert.DeserializeObject<List<BadgeModel>>(badges);
                    }
                }
                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserModel();
            return empty;
        }

        public UserModel UserSignUp(NewUserModel newUser)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var path = $"{_domain}/usersignup";
                var username = _authKey;
                var password = "";

                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));
                var client = new RestClient(path);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddParameter("first_name", newUser.First_Name);
                request.AddParameter("last_name", newUser.Last_Name);
                request.AddParameter("email", newUser.Email);
                request.AddParameter("login", newUser.Login);
                request.AddParameter("password", newUser.Password);
                if (newUser.Branch_Id.HasValue) request.AddParameter("branch_id", newUser.Branch_Id);
                if (newUser.Group_Id.HasValue) request.AddParameter("group_id", newUser.Group_Id);

                IRestResponse response = client.Execute(request);

                var result = response.Content;
                var user = new UserModel();
                user = JsonConvert.DeserializeObject<UserModel>(result);
                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserModel();
            return empty;
        }

        public UserModel EditUser(EditUserModel user)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var path = $"{_domain}/edituser";
                var username = _authKey;
                var password = "";

                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));
                var client = new RestClient(path);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddParameter("user_id", user.User_Id);
                if (user.First_Name != "") request.AddParameter("first_name", user.First_Name);
                if (user.Last_Name != "") request.AddParameter("last_name", user.Last_Name);
                if (user.Email != "") request.AddParameter("email", user.Email);
                if (user.Login != "") request.AddParameter("login", user.Login);
                if (user.Password != "") request.AddParameter("password", user.Password);
                if (user.Bio != "") request.AddParameter("bio", user.Bio);
                if (user.Timezone != "") request.AddParameter("timezone", user.Timezone);
                if (user.Credits.HasValue) request.AddParameter("credits", user.Credits);

                IRestResponse response = client.Execute(request);

                var result = response.Content;
                var updatedUser = new UserModel();
                updatedUser = JsonConvert.DeserializeObject<UserModel>(result);
                return updatedUser;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserModel();
            return empty;
        }

        public MessageModel DeleteUser(int userId, int? deletedByUserId)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var path = $"{_domain}/deleteuser";
                var username = _authKey;
                var password = "";

                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));
                var client = new RestClient(path);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddParameter("user_id", userId);
                if (deletedByUserId.HasValue) request.AddParameter("deleted_by_userId", deletedByUserId);

                IRestResponse response = client.Execute(request);

                var result = response.Content;
                var message = new MessageModel();
                message = JsonConvert.DeserializeObject<MessageModel>(result);
                return message;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new MessageModel();
            return empty;
        }

        public UserLoginModel UserLogin(string login, string password, string logoutRedirect)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var path = $"{_domain}/userlogin";
                var username = _authKey;
                var authPassword = "";

                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{authPassword}"));
                var client = new RestClient(path);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddParameter("login", login);
                request.AddParameter("password", password);
                if (logoutRedirect != "") request.AddParameter("logout_redirect", Base64Encode(logoutRedirect));

                IRestResponse response = client.Execute(request);

                var result = response.Content;
                var userLogin = new UserLoginModel();
                userLogin = JsonConvert.DeserializeObject<UserLoginModel>(result);
                return userLogin;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserLoginModel();
            return empty;
        }

        public UserLogoutModel UserLogout(int userId, string next)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var path = $"{_domain}/userlogout";
                var username = _authKey;
                var authPassword = "";

                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{authPassword}"));
                var client = new RestClient(path);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddParameter("user_id", userId);
                if (next != "") request.AddParameter("next", next);

                IRestResponse response = client.Execute(request);

                var result = response.Content;
                var userLogout = new UserLogoutModel();
                userLogout = JsonConvert.DeserializeObject<UserLogoutModel>(result);
                return userLogout;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserLogoutModel();
            return empty;
        }

        public UserStatusModel SetUserStatus(int userId, string status)
        {
            try
            {
                var path = $"{_domain}/usersetstatus/user_id:{userId},status:{status}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var userStatus = new UserStatusModel();
                userStatus = JsonConvert.DeserializeObject<UserStatusModel>(result);
                return userStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserStatusModel();
            return empty;
        }

        public ForgotUsernamePasswordModel ForgotUsername(string email, string domainUrl = "")
        {
            try
            {
                var path = domainUrl != "" ? $"{_domain}/forgotusername/email:{email},domain_url:{Base64Encode(domainUrl)}" : $"{_domain}/forgotusername/email:{email}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var userStatus = new ForgotUsernamePasswordModel();
                userStatus = JsonConvert.DeserializeObject<ForgotUsernamePasswordModel>(result);
                return userStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new ForgotUsernamePasswordModel();
            return empty;
        }

        public ForgotUsernamePasswordModel ForgotPassword(string username, string domainUrl = "", string redirectUrl = "")
        {
            try
            {
                var path = "";
                if (domainUrl != "")
                {
                    if (redirectUrl != "")
                        path = $"{_domain}/forgotpassword/username:{username},domain_url:{Base64Encode(domainUrl)},redirect_url:{Base64Encode(redirectUrl)}";
                    else
                        path = $"{_domain}/forgotpassword/username:{username},domain_url:{Base64Encode(domainUrl)}";
                }
                else if (redirectUrl != "")
                    path = $"{_domain}/forgotpassword/username:{username},redirect_url:{Base64Encode(redirectUrl)}";
                else
                    path = $"{_domain}/forgotpassword/username:{username}";
                
                var response = AuthorizeGet(path);
                var result = response.Content;
                var userStatus = new ForgotUsernamePasswordModel();
                userStatus = JsonConvert.DeserializeObject<ForgotUsernamePasswordModel>(result);
                return userStatus;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new ForgotUsernamePasswordModel();
            return empty;
        }
    }
}
