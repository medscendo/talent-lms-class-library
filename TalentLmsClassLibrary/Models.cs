﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace TalentLmsClassLibrary
{
    public class CoursesModel
    {
        public List<CourseModel> Courses { get; set; }
    }

    public class CourseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? Category_Id { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string Status { get; set; }
        public string Creation_Date { get; set; }
        public string Last_Update_On { get; set; }
        public int Creator_Id { get; set; }
        public string Hide_From_Catalog { get; set; }
        public string Time_Limit { get; set; }
        public int? Level { get; set; }
        public string Shared { get; set; }
        public string Shared_Url { get; set; }
        public string Avatar { get; set; }
        public string Big_Avatar { get; set; }
        public string Certification { get; set; }
        public string Certification_Duration { get; set; }
        public CourseUsersModel CourseUsersModel { get; set; }
        public UnitsModel UnitsModel { get; set; }
        public List<string> Rules { get; set; }
        public List<string> Prerequisites { get; set; }

        public CourseModel()
        {
            CourseUsersModel = new CourseUsersModel();
            UnitsModel = new UnitsModel();
        }
    }

    public class CourseUsersModel
    {
        public List<CourseUserModel> CourseUsers { get; set; }
    }

    public class CourseUserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Enrolled_On { get; set; }
        public string Enrolled_On_Timestamp { get; set; }
        public string Completed_On { get; set; }
        public string Completed_On_Timestamp { get; set; }
        public string Completion_Status { get; set; }
        public int? Completion_Percentage { get; set; }
        public string Expired_On { get; set; }
        public string Expired_On_Timestamp { get; set; }
        public string Total_Time { get; set; }
    }

    public class UrlModel
    {
        public string Goto_Url { get; set; }
    }

    public class UserStatusModel
    {
        public int User_Id { get; set; }
        public string Status { get; set; }
    }

    public class UserLoginModel
    {
        public int User_Id { get; set; }
        public string Login_Key { get; set; }
    }

    public class UserLogoutModel
    {
        public string Redirect_Url { get; set; }
    }

    public class ForgotUsernamePasswordModel
    {
        public int User_Id { get; set; }
    }

    public class MessageModel
    {
        public string Message { get; set; }
    }

    public class NewUserModel
    {
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int? Branch_Id { get; set; }
        public int? Group_Id { get; set; }
    }

    public class EditUserModel
    {
        public int User_Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Bio { get; set; }
        public string Timezone { get; set; }
        public int? Credits { get; set; }
    }

    public class UserRegistrationsModel
    {
        public List<UserRegistrationModel> UserRegistrations { get; set; }
    }

    public class UserRegistrationModel
    {
        public int User_Id { get; set; }
        public int Course_Id { get; set; }
        public string Role { get; set; }
    }

    public class UserRemoveRegistrationModel
    {
        public int User_Id { get; set; }
        public int Course_Id { get; set; }
        public string Course_Name { get; set; }
    }

    public class UsersModel
    {
        public List<UserModel> Users { get; set; }
    }

    public class UserModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public string User_Type { get; set; }
        public string Timezone { get; set; }
        public string Status { get; set; }
        public int? Level { get; set; }
        public int? Points { get; set; }
        public string Created_On { get; set; }
        public string Avatar { get; set; }
        public string Bio { get; set; }
        public string Login_Key { get; set; }
        public CourseUsersModel CourseUsersModel { get; set; }
        public BranchesModel BranchesModel { get; set; }
        public GroupsModel GroupsModel { get; set; }
        public CertificationsModel CertificationsModel { get; set; }
        public BadgesModel BadgesModel { get; set; }

        public UserModel()
        {
            CourseUsersModel = new CourseUsersModel();
            BranchesModel = new BranchesModel();
            GroupsModel = new GroupsModel();
            CertificationsModel = new CertificationsModel();
            BadgesModel = new BadgesModel();
        }
    }

    public class BranchesModel
    {
        public List<BranchModel> Branches { get; set; }
    }

    public class BranchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class GroupsModel
    {
        public List<GroupModel> Groups { get; set; }
    }

    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CertificationsModel
    {
        public List<CertificationModel> Certifications { get; set; }
    }

    public class CertificationModel
    {
        public int Course_Id { get; set; }
        public string Course_Name { get; set; }
        public string Unique_Id { get; set; }
        public string Issued_Date { get; set; }
        public string Expiration_Date { get; set; }
        public string Download_Url { get; set; }
    }

    public class BadgesModel
    {
        public List<BadgeModel> Badges { get; set; }
    }

    public class BadgeModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Criteria { get; set; }
        public string Issued_On { get; set; }
    }

    public class UnitsModel
    {
        public List<UnitModel> Units { get; set; }
    }

    public class UnitModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }

}