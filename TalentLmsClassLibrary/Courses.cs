﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net;

namespace TalentLmsClassLibrary
{
    public class Courses
    {
        private string _authKey;
        private string _domain;

        public Courses(string domain, string authKey)
        {
            _domain = domain;
            _authKey = authKey;
        }

        private IRestResponse AuthorizeGet(string endpoint)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var path = endpoint;
            var username = _authKey;
            var password = "";

            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));
            var client = new RestClient(path);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", $"Basic {encoded}");
            IRestResponse response = client.Execute(request);

            return response;
        }

        public CoursesModel GetCourses()
        {
            try
            {
                var path = $"{_domain}/courses";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var courses = new CoursesModel();
                courses.Courses = JsonConvert.DeserializeObject<List<CourseModel>>(result);
                return courses;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new CoursesModel();
            return empty;
        }

        public CourseModel GetCourse(int id)
        {
            try
            {
                var path = $"{_domain}/courses/id:{id}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var course = new CourseModel();
                course = JsonConvert.DeserializeObject<CourseModel>(result);
                JObject o = JObject.Parse(result);
                foreach (var item in o)
                {
                    if (item.Key == "users")
                    {
                        var users = item.Value.ToString();
                        course.CourseUsersModel.CourseUsers = JsonConvert.DeserializeObject<List<CourseUserModel>>(users);
                    }
                    if (item.Key == "units")
                    {
                        var units = item.Value.ToString();
                        course.UnitsModel.Units = JsonConvert.DeserializeObject<List<UnitModel>>(units);
                    }
                }
                return course;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new CourseModel();
            return empty;
        }

        public UrlModel GoToCourse(int userId, int courseId)  //has 3 optional paramters not implemented
        {
            try
            {
                var path = $"{_domain}/gotocourse/user_id:{userId},course_id:{courseId}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var goto_url = JsonConvert.DeserializeObject<UrlModel>(result);
                return goto_url;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var url = new UrlModel();
            return url;
        }

        public UserRegistrationsModel AddUserToCourse(UserRegistrationModel userRegistration)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var path = $"{_domain}/addusertocourse";
                var username = _authKey;
                var authPassword = "";

                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{authPassword}"));
                var client = new RestClient(path);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");
                request.AddHeader("Authorization", $"Basic {encoded}");
                request.AddParameter("user_id", userRegistration.User_Id);
                request.AddParameter("course_id", userRegistration.Course_Id);
                if (userRegistration.Role != "") request.AddParameter("role", userRegistration.Role);

                IRestResponse response = client.Execute(request);

                var result = response.Content;
                var registeredUser = new UserRegistrationsModel();
                registeredUser.UserRegistrations = JsonConvert.DeserializeObject<List<UserRegistrationModel>>(result);
                return registeredUser;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var empty = new UserRegistrationsModel();
            return empty;
        }

        public UserRemoveRegistrationModel RemoveUserFromCourse(int userId, int courseId)  //has 3 optional paramters not implemented
        {
            try
            {
                var path = $"{_domain}/removeuserfromcourse/user_id:{userId},course_id:{courseId}";
                var response = AuthorizeGet(path);
                var result = response.Content;
                var removeResult = JsonConvert.DeserializeObject<UserRemoveRegistrationModel>(result);
                return removeResult;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            var url = new UserRemoveRegistrationModel();
            return url;
        }
    }
}
